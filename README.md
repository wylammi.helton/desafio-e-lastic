<div>
<h1>Case - Dev React Native</h1>

## 👋 Intro
O objetivo desse exercício é construir um aplicativo em React Native para os fisioterapeutas realizarem avaliações de força em seus pacientes. 


## 📱 Telas
| ![Home](./.gitlab/Exercicios.png "Home") |  ![Home](./.gitlab/Home-vertical.png "Home") | ![Home-Horizontal](./.gitlab/Home-horizontal.png "Home Horizontal") |![Drawer](./.gitlab/modal.png "Modal") |![Drawer](./.gitlab/drawer.png "Drawer")|
| :-------------------------------------------------------: | :-------------------------------------------------------: |:---------------------------------------------------: | :---------------------------------------------------: |:---------------------------------------------------: |


## 👾 Tecnologias usadas
- __Linguagem__
    - [Typescript](https://github.com/microsoft/TypeScript) 
- __Arquitetura Flux__
    - [Redux](https://redux.js.org/) 
- __Roteamento e navegação__
    - [React Navigation](https://reactnavigation.org/) 
- __UI__
    - [Native Paper](https://callstack.github.io/react-native-paper/) usado no drawer navigation
    - [Styled Components](https://github.com/styled-components/styled-components)
- __Animações__
    - [React Native Animated](https://reactnative.dev/docs/animated)
- __Gráficos__
    - [React Native Chart Kit](https://github.com/indiespirit/react-native-chart-kit)
---

## 😣 Maiores dificuldades
    - Adptar o layout do app, pois nunca tinha precisado antes fazer um app que se
     comportasse de maneiras diferentes de acordo com a rotação da tela. Então de certa
     forma foi algo novo para mim.
     
## 😅 O que não consegui fazer
    - Deixar a aplicação dinâmica como pegar dados de um server real, pois simulei as funcionalidades dentro do próprio APP.
</div>

