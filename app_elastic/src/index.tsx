import 'react-native-gesture-handler';
import React from 'react';
import { StatusBar, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Routes from './routes';
import { colors } from './styles';
import ChartModal from './Components/Modals/chart_modal';
import { Provider } from 'react-redux';
import store from './store';
import SelectExercise from './Components/Modals/select_exercise';

const App: React.FC = () => {
  return (
    <View style={{ flex: 1 }}>
      <Provider store={store}>
        <NavigationContainer>
          <StatusBar
            barStyle={'light-content'}
            backgroundColor={colors.primary}
          />
          <Routes />
          <ChartModal />
          <SelectExercise />
        </NavigationContainer>
      </Provider>
    </View>
  );
};

export default App;
