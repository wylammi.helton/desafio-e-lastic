export default {
  primary: '#e5005b',
  white: '#f5f5f5',
  silver: '#A09F9F',
  silver_black: '#5a5858',
};
