export const Types = {
  SET_CURRENT_EXCERCISE: 'SET_CURRENT_EXCERCISE',
};

const INITIAL_STATE = {
  excersices: [
    {
      id: 1,
      title: 'Abdominal',
    },
    {
      id: 2,
      title: ' Flexão de cotovelo',
    },
    {
      id: 3,
      title: ' Extensão do cotovelo direito',
    },
    {
      id: 4,
      title: ' Extensão do cotovelo esquerdo',
    },
  ],
  current_exercise: null,
};

export default function excersices(state = INITIAL_STATE, action: any) {
  switch (action.type) {
    case Types.SET_CURRENT_EXCERCISE:
      return {
        ...state,
        current_exercise: action.payload,
      };

    default:
      return state;
  }
}

export const ExcerciseActions = {
  set_current_excercise: (exercise: any) => ({
    type: Types.SET_CURRENT_EXCERCISE,
    payload: exercise,
  }),
};
