import React from 'react';
import Modal from 'react-native-modal';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import { colors, metrics } from '../../styles';
import { LineChart } from 'react-native-chart-kit';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AppActions } from '../../store/ducks/app';

const data = {
  labels: ['0.0', '1.0', '2.0', '3.0', '4.0'],
  datasets: [
    {
      data: [0, 5, 25, 40, 20, 0],
      color: (opacity = 1) => `rgb(255, 0, 88)`, // optional
      strokeWidth: 2,
    },
  ],
  legend: ['Tempo de Contração'], // optional
};
const chartConfig = {
  backgroundGradientFrom: '#fff',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: colors.white,
  color: (opacity = 1) => `rgb(0, 0, 0)`,
};

export default function ChartModal() {
  const visible = useSelector((state: any) => state.app.visible);
  const screenWidth = Dimensions.get('window').width;

  const dispatch = useDispatch();
  return (
    <Modal style={{ alignItems: 'center' }} isVisible={visible}>
      <View style={styles.container}>
        <Text style={styles.title}>Série concluída com sucesso!</Text>
        <LineChart
          style={{ marginTop: 5 }}
          data={data}
          width={screenWidth * 0.8}
          height={200}
          chartConfig={chartConfig}
          bezier
        />
        <Text style={{ textAlign: 'center' }}>Tempo (s)</Text>
        <View style={{ marginTop: 15, width: '100%', paddingHorizontal: 10 }}>
          <Text style={styles.force}>
            Forçã Máxima: <Text style={styles.force2}>40.00 Kg</Text>
          </Text>
          <Text style={styles.force}>
            Forçã Média: <Text style={styles.force2}>40.00 Kg</Text>
          </Text>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => dispatch(AppActions.closeModal())}>
          <Icon name={'check'} size={20} color={colors.white} />
          <Text
            style={{
              marginLeft: 5,
              color: colors.white,
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Continuar
          </Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '98%',
    height: metrics.screenHeight * 0.7,
    borderRadius: 20,
    paddingHorizontal: 20,
    alignItems: 'center',
    paddingTop: 20,
    backgroundColor: colors.white,
  },
  title: {
    fontSize: 22,
    color: colors.silver_black,
    fontWeight: 'bold',
    textAlign: 'left',
  },
  force: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.silver_black,
  },
  force2: {
    fontSize: 18,
    fontWeight: '600',
    color: colors.silver_black,
  },
  button: {
    width: '90%',
    height: 45,
    marginTop: 30,
    borderRadius: 8,
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
