import React from 'react';
import Home from './Pages/Home';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { DrawerContent } from './Components/Drawer';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default function Routes() {
  function StackHome() {
    return (
      <Stack.Navigator headerMode="none" initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
      </Stack.Navigator>
    );
  }

  return (
    <Drawer.Navigator
      drawerContent={(props) => <DrawerContent {...props} />}
      initialRouteName="Home">
      <Drawer.Screen name="Home" component={StackHome} />
    </Drawer.Navigator>
  );
}
